{extends file='page.tpl'}

{block name='page_title'}
  {$cms.meta_title}
{/block}

{block name='left_column'}
  <div id="left-column" class="col-xs-12 col-sm-3">
    {widget name="ps_contactinfo" hook='displayLeftColumn'}
  </div>
{/block}
<div id="r-column" class="col-xs-12 col-sm-9">
	{block name='page_content_container'}
	  <section id="content" class="page-content page-cms page-cms-{$cms.id}">
	    {block name='page_content'}
	      {widget name="contactform"}
	    {/block}
	  </section>
	{/block}
</div>
