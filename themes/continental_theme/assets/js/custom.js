/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

 (function($){
    // Form dealer customitation
 	if($('body').hasClass('cms-id-6')){
 		var form_wrapper = $('.contact-form form'),
 			fields_wrapper = form_wrapper.find('.form-fields'),
 			field_wrapper = fields_wrapper.find('.form-group.row');

 		field_wrapper.each(function(i,e){
 			
 			if (i == 1){
 				var index_instance = $(this),
 					input_wrapper = index_instance.find('.col-md-6'),
 					select_instance = input_wrapper.find('select');
 				
 				if(select_instance) {
 					//select_instance.find('option').remove();
 					//select_instance.append('<option value="2">Ser Distribuidor</option>');
 				}
 			
 			}

 		});
 	}

 	$('.featured-products h2.products-section-title.text-uppercase').html('Productos Destacados');

 	// Reasurance links
 	var reasurance_wrapper = $('#block-reassurance'),
 		reasurance_links   = $('#block-reassurance ul li');	

 		if(reasurance_wrapper) {
			reasurance_links.each(function(index, el) {
				var div_instance = $(this).find('div');

				if(index == 0) {
					div_instance.wrapAll('<a href="/adjuntos/politicas_garantia.pdf" target="_blank"></a>');
				}
				if(index == 1) {
					div_instance.wrapAll('<a href="/adjuntos/politica_de_envio.pdf" target="_blank"></a>');
				}
			}); 			
		 }
	
	// Footer links
	var linkList = $('.footer-container .links .collapse');

	
	linkList.each(function(i,e){
		if (i == 1){
			var instanceWrapper = $(this);
			var instanceWrapperLinks = instanceWrapper.find('li');

			instanceWrapperLinks.each(function(i,e){
				if (i == 4 || i == 5){
					$(this).find('a').attr('target','_blank');
				}
			});
		}
	})	 
		
	

	// Accordion items
	var AccordionWrapper = $('section.facet');

	AccordionWrapper.each(function(index, el) {

		var instance = $(this);
		var clickedText = instance.find('p');
		var childrenList = instance.find('ul');
		var clickedTextName = clickedText.text();

		instance.addClass(clickedTextName);

		
		childrenList.find('li').each(function(index, el) {
			var instanceli = $(this);
			var listItems = instanceli.find('a').text();
			instanceli.addClass(listItems);
		});

		clickedText.on('click', function(event) {
			/* Act on the event */
			$(this).toggleClass('active-clicked');
			childrenList.slideToggle();
		});

	});

	// -- Map -- //
	const $MapScope = {
		// Constructor
		init: function() {
			this.mapScripts();
		},
		mapScripts: function() {

			let map = L.map('Map', {
					center: [4.6407,-74.0779],
					zoom: 4.5
				});

			let mapRender = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);

			
			// Set markers
			let MarkerPointer = $('.side-map__side.list ul li');

			if(MarkerPointer) {
				MarkerPointer.each(function(index, el) {

					let instance = $(this);

					// Map item vars
					let latLen = instance.find('.location').html();
					let itemTitle = instance.find('.name').html();
					let itemAddress = instance.find('.address').html();
					let itemPhone = instance.find('.phone').html();

					let latSplited = latLen.split(",");
					let Lat = parseFloat(latSplited[0]);
					let Len = parseFloat(latSplited[1]);
					let div_circle = L.divIcon({ className: 'circle'});

					console.log(Lat, Len, 'lat len')

					let markerItem = L.marker([Lat,Len],{icon: div_circle}).addTo(map).bindPopup('<h3>'+ itemTitle +'</h3><p>'+itemAddress+'</p>');

				});
			}
		},
	}

	if($('body').hasClass('cms-id-7')){
		$MapScope.init();
	}


 })(jQuery)