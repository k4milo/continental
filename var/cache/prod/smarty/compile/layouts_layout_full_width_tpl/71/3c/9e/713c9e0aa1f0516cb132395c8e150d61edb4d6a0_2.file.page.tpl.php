<?php
/* Smarty version 3.1.32, created on 2018-07-17 06:30:57
  from '/home/pruebascontinent/public_html/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b4dc5618f6511_34596982',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '713c9e0aa1f0516cb132395c8e150d61edb4d6a0' => 
    array (
      0 => '/home/pruebascontinent/public_html/themes/classic/templates/page.tpl',
      1 => 1530670900,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b4dc5618f6511_34596982 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13129237255b4dc5618dea66_58446906', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_17888618655b4dc5618e1817_75340446 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_2550420835b4dc5618dfe68_14765697 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17888618655b4dc5618e1817_75340446', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_3795170955b4dc5618f1e49_89177552 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_19313593695b4dc5618f2c77_37626979 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_16698226305b4dc5618f13f0_25273829 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3795170955b4dc5618f1e49_89177552', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19313593695b4dc5618f2c77_37626979', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_9264798345b4dc5618f4c67_34148256 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_10358012935b4dc5618f42d0_20331530 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9264798345b4dc5618f4c67_34148256', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_13129237255b4dc5618dea66_58446906 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13129237255b4dc5618dea66_58446906',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_2550420835b4dc5618dfe68_14765697',
  ),
  'page_title' => 
  array (
    0 => 'Block_17888618655b4dc5618e1817_75340446',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_16698226305b4dc5618f13f0_25273829',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_3795170955b4dc5618f1e49_89177552',
  ),
  'page_content' => 
  array (
    0 => 'Block_19313593695b4dc5618f2c77_37626979',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_10358012935b4dc5618f42d0_20331530',
  ),
  'page_footer' => 
  array (
    0 => 'Block_9264798345b4dc5618f4c67_34148256',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2550420835b4dc5618dfe68_14765697', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16698226305b4dc5618f13f0_25273829', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10358012935b4dc5618f42d0_20331530', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
