<?php
/* Smarty version 3.1.32, created on 2018-07-17 06:30:57
  from '/home/pruebascontinent/public_html/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b4dc5618b42d5_31659556',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '91171cd53d793c7a9caf8823b3f9a1506c24e51d' => 
    array (
      0 => '/home/pruebascontinent/public_html/themes/classic/templates/index.tpl',
      1 => 1530670900,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b4dc5618b42d5_31659556 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20315847395b4dc5618af245_44407056', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_10356644275b4dc5618afee8_82424989 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_8701104285b4dc5618b1ac3_47773647 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_7710895625b4dc5618b1025_43490370 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8701104285b4dc5618b1ac3_47773647', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_20315847395b4dc5618af245_44407056 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_20315847395b4dc5618af245_44407056',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_10356644275b4dc5618afee8_82424989',
  ),
  'page_content' => 
  array (
    0 => 'Block_7710895625b4dc5618b1025_43490370',
  ),
  'hook_home' => 
  array (
    0 => 'Block_8701104285b4dc5618b1ac3_47773647',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10356644275b4dc5618afee8_82424989', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7710895625b4dc5618b1025_43490370', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
