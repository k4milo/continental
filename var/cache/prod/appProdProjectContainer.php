<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerRrzi6be\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerRrzi6be/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerRrzi6be.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerRrzi6be\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerRrzi6be\appProdProjectContainer(array(
    'container.build_hash' => 'Rrzi6be',
    'container.build_id' => '1d9abafd',
    'container.build_time' => 1531823514,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerRrzi6be');
